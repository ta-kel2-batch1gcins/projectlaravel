@extends('layout')

@section('content')
  <center style="margin-top: -20px"><h2>FORUM TANYA JAWAB</h2></center><br><h4 style="margin-left: -50px">Popular Posts</h4><div style="margin-left: -50px">(Method: filtered by Admin)</div>
    <!-- Main content -->
    <div class="content" style="margin-left: -50px">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">username</h5><br>
                <h5 class="card-title">Question</h5><br>

                <p class="card-text">
                  Some quick example text to build on the card title and make up the bulk of the card's
                  content.
                </p>

                <b>1K</b><a href="#" class="card-link ml-2">Total Comments</a>
                <a href="#" class="card-link">#Hashtag</a>
                <a href="#" class="card-link">#Hashtag2</a>
                <a href="#" class="card-link">#Hashtag3</a>
                <a href="#" class="card-link">#Hashtag4</a>
              </div>
            </div>

            <div class="card card card-outline">
              <div class="card-body">
                <h5 class="card-title">username2</h5><br>
                <h5 class="card-title">Question</h5><br>

                <p class="card-text">
                  Some quick example text to build on the card title and make up the bulk of the card's
                  content.
                </p>
                  <b>113</b> <a href="#" class="card-link ml-2">Total Comments</a>
                <a href="#" class="card-link">#Hashtag</a>
              </div>
            </div><!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-5 ml-5">            
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Hashtag Popular</h5>
              </div>
              <div class="card-body">
                <h5><a href="#" class="card-link">#name hashtag</a></h5><br>
                <h5><a href="#" class="card-link">#name hashtag</a></h5><br>
                <h5><a href="#" class="card-link">#name hashtag</a></h5><br>
                <h5><a href="#" class="card-link">#name hashtag</a></h5><br>            
                <a href="#" class="btn btn-primary">Lihat lebih banyak</a>
              </div>
            </div>

  
@endsection

@push('child-scripts')
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

