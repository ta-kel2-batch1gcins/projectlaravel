<!DOCTYPE html>
<html>
<head>
	<title>CRUD Film di Laravel</title>
</head>
<body>

	<h3>Data Pemain Film</h3>

	<a href="/cast/create"> + Tambah Pemain Film Baru</a>

	<br/>
	<br/>

	<table border="1">
		<tr>
			<th>Name</th>
			<th>Umur</th>
			<th>Bio</th>
			<th>Opsi</th>
		</tr>
		@foreach($cast as $c)
		<tr>
			<td>{{ $c->nama }}</td>
			<td>{{ $c->umur }}</td>
			<td>{{ $c->bio }}</td>
			<td>
				<a href="/cast/edit/{{ $c->id }}">Edit</a>
				|
				<script type="text/javascript">function confirm_click(){return confirm("Yakin hapus?");}
				</script>
				<a onclick="return confirm_click()" href="/cast/destroy/{{ $c->id }}">Hapus</a>

			</td>
		</tr>
		@endforeach
	</table>


</body>
</html>
