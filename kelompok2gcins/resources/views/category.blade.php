@extends('layout')

@section('content')
<div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="float-sm-right">Category</h1>
          </div>
          <div class="col-sm-6 mt-5">
            <a href="/category/create" class="btn btn-sm btn-primary addkat">Add</a>
          </div>
        </div>

<table id="example2" class="table table-bordered table-hover " data-page-length='5'>
<thead class="dboard-z1">
<tr>
<th>No</th>
<th>Username</th>
<th>Judul</th>
<th>Hashtag</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $nomor=1; ?>
@foreach($category as $c)
<tr>
<td>{{ $nomor }}</td>
<td>{{ $c->users_id }} <= Username ID</td>
<td>{{ $c->judul }}</td>
<td>{{ $c->hashtag }}</td>
<td>
<a href="/category/edit/{{ $c->id }}" class="btn btn-sm btn-warning">edit</a>
<script type="text/javascript">function confirm_click(){return confirm("Warning: Data yang dipilih akan dihapus. Lanjut menghapus?");}
</script>
<a onclick="return confirm_click()" href="/category/destroy/{{ $c->id }}" class="btn btn-sm btn-danger">delete</a>
</td>
</tr>
<?php $nomor += 1 ?>
@endforeach
</tbody>
</table>
@endsection

@push('child-scripts')
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush

