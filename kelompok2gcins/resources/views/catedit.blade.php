<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CRUD Film di Laravel</title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body style="text-align: center"><br>

	<h3>Edit Category</h3>


	@foreach($category as $c)
	<form action="/cast/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $c->users_id }}"> <br/>
        <label for='firstname'>Judul :</label><br><br>
		<textarea name="bio" cols="30" rows="6">{{ $c->judul }}</textarea> <br><br>
		<label for='firstname'>Hashtag :</label><br><br>
		<textarea name="bio" cols="30" rows="6">{{ $c->hashtag }}</textarea> <br><br>
		<input type="submit" value="Update Data">
	</form>
	@endforeach
    <a href="/category"> Kembali</a>
</body>
</html>
