<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    //membuat function login dan register
    public function login()
    {
        return view('login');
    }

    public function register()
    {
        return view('register');
    }
}
