<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    //
    public function index()
    {
        if(Auth::check()){
        // mengambil data dari table category
        $category = DB::table('category')->get();

        // mengirim data category ke view index
        return view('category', ['category' => $category]);
        }
        
        return redirect("login")->withSuccess('Mohon login untuk melanjutkan ke website');
    }

    public function loginname()
    {
        // mengambil data dari table category
        $name = DB::table('users')->get();

        // mengirim data category ke view index
        return view('category', ['uname' => $name]);
    }

    public function create()
    {

        // memanggil view create
        return view('catcreate');
    }

    // method untuk insert data ke table category
    public function store(Request $request)
    {
        // insert data ke table category
        $dt = new DateTime(); //get current datetime
        $ht = $request->hashtag; //hashtag string handling
        $ht = str_replace("#","",$ht);
        $array = preg_split('/\s+/', $ht);
        $ht = "";
        for($i=0; count($array) > $i; $i++){
            if(substr($array[$i],0,1) <> "#"){
                $ht = $ht."#".$array[$i]." ";
            }            
        }
        DB::table('category')->insert([
            'judul' => $request->judul,
            'hashtag' => $ht,
            'users_id' => Auth::user()->id,
            'timestamp' => $dt
        ]);
        // alihkan ke halaman index
        return redirect('/category');
    }

    // method untuk edit data pemain film
    public function edit($id)
    {
        // mengambil data pemain film berdasarkan id yang dipilih
        $category = DB::table('category')->where('id', $id)->get();
        // passing data pemain film yang didapat ke view edit.blade.php
        return view('catedit', ['category' => $category]);
    }

    // update data pegawai
    public function update(Request $request)
    {
        // update data judul dan hashtag
        DB::table('category')->where('id', $request->id)->update([
            'judul' => $request->judul,
            'hashtag' => $request->hashtag,
        ]);
        // alihkan halaman ke halaman index
        return redirect('/category');
    }

    // method untuk hapus data pemain film
    public function destroy($id)
    {
        // menghapus data pemain film berdasarkan id yang dipilih
        DB::table('category')->where('id', $id)->delete();

        // alihkan halaman ke halaman index
        return redirect('/category');
    }
}
